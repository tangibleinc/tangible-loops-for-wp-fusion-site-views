<?php	?>
<Loop type=post count=10>
  <If user_field=wp_fusion_access>
    <h1><Field title /></h1>
    <div><Field content /></div>

    <If user_field=wp_fusion_tags  includes value="Cannot_See_Slug">
      <h5 style="color:red">User can not see Post slug</h5>
      <Else />
      <h5> Slug:</h5>
      <Field name="slug"/>
      </p>
    </If>

  <Else />
    <div style="padding:40px 10px; margin:50px 0; border:1px solid #dee2e6;">
      <h2 style="color:red; border:none;">User does not have access.</h2>
      <p >Post <strong> "<Field title />"</strong> is protected by CRM - required tag 'Allowed_Access_Demo2'. User does not have the required tag applied. </p>
    </div>
	</If>
</Loop>


<h1>The Whole Page Template Markup:</h1>
<Markdown>
```html
Post Archive Markup.

<!-- Loop through post items -->
<Loop type=post count=10>

  <!-- If user has access to a single post item -->
  <If user_field=wp_fusion_access>

    <!-- Post item fields-->
    <h1><Field title /></h1>
    <div><Field content /></div>

    <!-- Post item field displayed conditionally -->
    <If user_field=wp_fusion_tags  includes value="Cannot_See_Slug">
      <h5 style="color:red">User can not see Post slug</h5>
      <Else />
      <h5> Slug:</h5>
      <Field name="slug"/>
      </p>
    </If>

  <!-- User does not access to a single post item -->
  <Else />
    <div style="padding:40px 10px; margin:50px 0; border:1px solid #dee2e6;">
      <h2 style="color:red; border:none;">User does not have access.</h2>
      <p >Post <strong> "<Field title />"</strong> is protected by CRM - required tag 'Allowed_Access_Demo2'. User does not have the required tag applied. </p>
    </div>
  </If>
</Loop>
```

********************************************************************
</Markdown>



<h2>Available tags for 'ExamplePagesUser' User:</h2>
<Loop type=user name="examplepagesuser">
  <Loop field=wp_fusion_tags>
    WP Fusion tag ID: <Field /><br>
  </Loop>
</Loop>
