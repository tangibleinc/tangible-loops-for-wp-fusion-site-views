<?php ?>
<If loop exists type=post name="{Get route part=2}">

  <Loop>
    <If user_field=wp_fusion_access>

      <!-- Post Title on condition -->
      <If user_field=wp_fusion_tags includes value="Cannot_See_Title">
        <h2 style="color:red">USER CANNOT SEE THE POST TITLE</h2>
        <Else />
        <h1>Post: <Field title /></h1>
      </If>

<Markdown>
```html
Conditional Post Title Template Markup. Condition: 'includes'. Tag: 'Cannot_See_Title' .

<If user_field=wp_fusion_tags includes value="Cannot_See_Title">
  <h2 style="color:red">USER CANNOT SEE THE POST TITLE</h2>
  <Else />
  <h1>Post: <Field title /></h1>
</If>
```
</Markdown>

<!-- Content on condition -->
      <If user_field=wp_fusion_tags any_starts_with value="Membership">
        <Loop type=post name="{Get route part=2}">
          <h2> Content:</h2>
          <Field content />
        </Loop>
      </If>

<Markdown>
```html
Conditional Content Template Markup. Condition: 'any_starts_with'. Tag: 'Membership Premium' .

<If user_field=wp_fusion_tags any_starts_with value="Membership">
  <Loop type=post name="{Get route part=2}">
    <h2> Content:</h2>
    <Field content />
  </Loop>
</If>
```
</Markdown>

<!-- Excerpt on condition -->
      <If user_field=wp_fusion_tags any_is value="Can_See_Excerpt">
        <Loop type=post name="{Get route part=2}">
          <h2> Excerpt:</h2>
          <Field excerpt/>
        </Loop>
      </If>


<Markdown>
```html
Conditional Excerpt Template Markup. Condition: 'any_is'. Tag: 'Can_See_Excerpt' .

<If user_field=wp_fusion_tags any_is value="Can_See_Excerpt">
  <Loop type=post name="{Get route part=2}">
    <h2> Excerpt:</h2>
    <Field excerpt/>
  </Loop>
</If>
```
</Markdown>

<!-- Author on condition  any_ends_with -->
      <If user_field=wp_fusion_tags any_ends_with value="Premium">
        <Loop type=post name="{Get route part=2}">
          <h2> Author Contact:</h2>
          <strong>Name:</strong> <Field name="author_name"/><br />
          <strong>Email:</strong> <Field name="author_email"/>
        </Loop>
      </If>



<Markdown>
```html
Conditional Author Contact Template Markup. Condition:'any_ends_with'. Tag: 'Membership Premium' .

<If user_field=wp_fusion_tags any_ends_with value="Premium">
  <Loop type=post name="{Get route part=2}">
    <h2> Author Contact:</h2>
    <strong>Name:</strong> <Field name="author_name"/><br />
    <strong>Email:</strong> <Field name="author_email"/>
  </Loop>
</If>
```
</Markdown>

<!-- Slug on condition -->
      <If user_field=wp_fusion_tags any_includes value="slug">
        <Loop type=post name="{Get route part=2}">
          <h2> Post Slug:</h2>
          <Field name="slug"/>
        </Loop>
      </If>


<Markdown>
```html
Conditional Post Slug Template Markup. Condition: 'any_includes'. Tag: 'Display-slug Optionally' .

<If user_field=wp_fusion_tags any_includes value="slug">
  <Loop type=post name="{Get route part=2}">
    <h2> Post Slug:</h2>
    <Field name="slug"/>
  </Loop>
</If>
```
</Markdown>


<h1>The Whole Page Template Markup:</h1>
<Markdown>
```html
Single Post Markup.

<!-- If post Loop exists -->
<If loop exists type=post name="{Get route part=2}">

  <!-- Start Loop -->
  <Loop>

    <!-- If current user has access to the post -->
    <If user_field=wp_fusion_access>

      <!-- Post Title on condition -->
      <If user_field=wp_fusion_tags includes value="Cannot_See_Title">
        <h2 style="color:red">USER CANNOT SEE THE POST TITLE</h2>
        <Else />
        <h1>Post: <Field title /></h1>
      </If>

      <!-- Content on condition -->
      <If user_field=wp_fusion_tags any_starts_with value="Membership">
        <Loop type=post name="{Get route part=2}">
          <h2> Content:</h2>
          <Field content />
        </Loop>
      </If>

      <!-- Excerpt on condition -->
      <If user_field=wp_fusion_tags any_is value="Can_See_Excerpt">
        <Loop type=post name="{Get route part=2}">
          <h2> Excerpt:</h2>
          <Field excerpt/>
        </Loop>
      </If>

      <!-- Author on condition -->
      <If user_field=wp_fusion_tags any_ends_with value="Premium">
        <Loop type=post name="{Get route part=2}">
          <h2> Author Contact:</h2>
          <strong>Name:</strong> <Field name="author_name"/><br />
          <strong>Email:</strong> <Field name="author_email"/>
        </Loop>
      </If>

      <!-- Slug on condition -->
      <If user_field=wp_fusion_tags any_includes value="slug">
        <Loop type=post name="{Get route part=2}">
          <h2> Post Slug:</h2>
          <Field name="slug"/>
        </Loop>
      </If>

      <!-- User does not have access-->
      <Else />
      <h1  style="color:red">User does not have access. </h1>
      <p >Post <strong> "<Field title />"</strong> is protected by CRM tag </p>
    </If>
  </Loop>

  <!-- Post Loop doesn't exist-->
  <Else />
  <Load file="/not-found.html" />
</If>
```

********************************************************************
</Markdown>


    <Else />
      <h1  style="color:red">User does not have access. </h1>
      <p >Post <strong> "<Field title />"</strong> is protected by CRM - required tag 'Allowed_Access_Demo2'. User does not have the required tag applied. </p>

      <h1>The Whole Page Template Markup:</h1>
<Markdown>
```html
Protected access to the Post Template Markup. Tag: 'Allowed_Access_Demo2' .

<!-- If post Loop Exists-->
<If loop exists type=post name="{Get route part=2}">

  <!-- Start Loop-->
  <Loop>

    <!-- Is User has access - render post-->
    <If user_field=wp_fusion_access>
      <!--
      ...
      Post content renders here
      ...
      -->

    <!-- User does not have access-->
    <Else />
      <h1  style="color:red">User does not have access. </h1>
      <p >Post <strong> "<Field title />"</strong> is protected by CRM - required tag 'Allowed_Access_Demo2'. User does not have the required tag applied. </p>
    </If>
  </Loop>
</If>

<!-- Post Loop doesn't exist-->
<Else />
  <Load file="/not-found.html" />
</If>
```
********************************************************************
</Markdown>

  </Loop>

<Else />
    <Load file="/not-found.html" />
</If>


<h2> Available tags for 'ExamplePagesUser' User:</h2>
<Loop type=user name="examplepagesuser">
  <Loop field=wp_fusion_tags>
      WP Fusion tag ID: <Field /><br>
  </Loop>
</Loop>









